#!/bin/sh
#
#-------------------------------------------------------------------------------
#
# Copyright (c) 2019 Tom L. Underwood
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
#--------------------------------------------------------------------------------
#
#                                blockav
#
# Author: Tom L. Underwood
#
# See 'display_help' below for information about this script, and 
# 'display_version' for information about the version (which should be
# updated by future developers).
#
# This script has been run through ShellCheck (https://www.shellcheck.net/) to
# check for POSIX compliance.
#
#



# Function definitions...

display_help() {
    echo
    echo "This script performs block averaging on a specified data file"
    echo "outputting the block-averaging mean and standard error to "
    echo "stdout (along with some other information pertaining to the"
    echo "data analysis). Note that the script ignores empty lines."
    echo
    echo "Usage: blockav [--debug] [-e|--equil <equil>] [-c|--column <col>] <size> <file>"
    echo "       blockav (-h|--help)"
    echo "       blockav (-v|--version)"
    echo
    echo "Here, <size> is the block size to use in the block averaging,"
    echo "and <file> is the name of the data file. <file> can be omitted"
    echo "if the data to be analysed by the script is provided from"
    echo "stdin."
    echo
    echo "<equil> is the equilibration period (default 0): the first"
    echo "<equil> data points are not used in the block averaging."
    echo 
    echo "<col> is the column number in the data file to apply the block"
    echo "averaging to (default 1)."
    echo 
    echo "The '--debug' flag can be used if the script is giving"
    echo "unexpected results (or crashes) and it is not clear why. In"
    echo "this case information is output to stdout to assist with"
    echo "determining why the script crashed. Specifically, the"
    echo "variables used in the script are output."
    echo
    echo "Example usage: blockav 100 energy.dat"
    echo "               blockav -e 100 -c 2 100 energy.dat"
    echo "               cat energy.dat | blockav -e 100 -c 2 100"
    echo

}


display_version() {
    echo "blockav version 0.1.1; 07/8/19"
}


debug_info() {
    echo
    echo "blockav debugging information:"
    echo "DEBUG  = $DEBUG"
    echo "EQUIL  = $EQUIL"
    echo "COLUMN = $COLUMN"
    echo "SIZE   = $SIZE"
    echo "FILE   = '$FILE'"
    echo
}




# Initilise default variables...

DEBUG=false
EQUIL=0
COLUMN=1
SIZE=""
FILE=""




# Read command-line arguments...

if [ $# = 0 ]
then

    display_help
    exit 1

elif [ "$1" = "-h" ] || [ "$1" = "--help" ]
then

    display_help
    exit 0

elif [ "$1" = "-v" ] || [ "$1" = "--version" ]
then

    display_version
    exit 0

fi


while [ "$1" != "" ]
do

    case $1 in

        # Debugging
        --debug)

            DEBUG=true
            shift
            ;;

        # Equilibration period
        -e|--equil)

            if [ "$2" != "" ]
            then
                EQUIL=$2
		if ! echo "$EQUIL" | grep -Eq '^[0-9]+$'
                then
		    echo "Error: Specified equilibration period '$EQUIL' is not a non-negative integer"
                    exit 1
		fi
            else
                echo "Error: No argument after '-e' or '--equil'"
                exit 1
	    fi
            shift
            shift
            ;;


        # Column number 
        -c|--column)
            if [ "$2" != "" ]
            then
                COLUMN=$2
		if ! echo "$COLUMN" | grep -Eq '^[0-9]+$'
                then
		    echo "Error: Specified column number '$COLUMN' is not a non-negative integer"
                    exit 1
		fi
            else
                echo "Error: No argument after '-c' or '--column'"
                exit 1
	    fi
            shift
            shift
            ;;

        
        # Otherwise we have the block size and file name
        *)

            SIZE=$1

            if ! echo "$SIZE" | grep -Eq '^[0-9]+$'
            then
	        echo "Error: Specified block size '$SIZE' is not a non-negative integer"
                exit 1
	    fi
            
            shift

            # Do not throw an error if the file name is not specified. If it is not
            # specified take input from stdin. Check the file exists if it is specified
            if [ "$1" != "" ]
            then

                FILE=$1
		if ! [ -f $FILE ]
		then

                    echo "Error: File '$FILE' not found"
                    exit 1		    

		fi
                shift

            else
             
                # Set input to stdin
                FILE="-"

	    fi

    esac

done




# Safety checks...


# First check that the block size has been specified
if [ "$SIZE" = "" ]
then
    echo "Error: Block size not specified. Try 'blockav --help'"
    exit 1
fi




# Print debugging information if necessary...


if [ $DEBUG = true ]
then

    debug_info

fi






# Perform the data analysis...

cat $FILE | awk -v equil=$EQUIL -v size=$SIZE -v col=$COLUMN '

    NF>0 {

        time++        

        if(time > equil){ 
            sum = sum + $col; 
            counts = counts + 1;
            gcounts = gcounts + 1;
            if(counts % size == 0){
                mean = sum / counts; 
                sum = 0; 
                counts = 0; 
                Sum = Sum + mean; 
                Sum2 = Sum2 + mean * mean; 
                Counts = Counts + 1;
            }
        }
    } 

    END{

        print "Total number of data points = ",time
        print "Equilibration period = ",equil
        print "Post-equilibration data points = ",gcounts
        print "Block size = ",size

        if(Counts == 0){
            print "Error: Did not reach end of one block."
            exit 1
        }

        print "Number of blocks found = ",Counts        

        Mean = Sum / Counts;
        printf "Block-averaging mean = %.16e\n",Mean
        
        if(Counts == 1){
            print "Error: Did not reach end of 2 blocks. Cannot calculate standard error."
            exit 1
        }
        else{
            Var = Sum2 / Counts - Mean * Mean;
            Stdev = sqrt(Var*Counts/(Counts-1))
            Stderr =  Stdev / sqrt(Counts);   
            printf "Block-averaving standard error = %.16e\n",Stderr
        }

    }

'
